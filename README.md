# wordpress-docker 
Zdockeryzowany Nginx, WordPress, MySQL i WP-CLI. Przyspiesza lokalny development. Inspiracja: https://github.com/mjstealey/wordpress-nginx-docker

UWAGA! Na razie nie jest możliwe odpalenie kilku instancji docker-compose. Żeby uruchomieć kolejną, poprzednią trzeba zatrzymać.

## Jak to działa
Wszystkie polecenia, które trzeba używać są w pliku Makefile.

### Używanie Makefile
Jak wyświetlić wszystkie dostępne polecenia? 
```
cat Makefile
```
Jak uruchomić kontenery?
```
make docker-start
```
Jak zatrzymać kontenery?
```
make docker-stop
```
Jak wyczyścić WordPressa?
```
make clean
```
Jak uruchomić kreatora instalacja CLI?
```
make wp-install
```
Jak uruchomić instalację w danych zdefiniowanych w plikach Makefile/.env?
```
make wp-install-prod
```
Jak przywrócić instalację z bazy danych? (dbpath relatywnie do ./wordpress)
```
make wp-install-db dbpath=path/to/db.sql
```
Jak ustawić uprawnienia do katalogów wordpressa dla www-data?
```
make wp-chmod
```
Jak dostać się do sh kontenera WP?
```
make wp-cli-sh
```

## Uruchomienie istniejącego Wordpressa
1.  Po pierwszym uruchomieniu kontenerów przejść nadpisać zawartość wordpress/wp-content. W przypadku projektu rozwijanego w git (tak jak airguru) sklonować zawartość repozytorium do katalogu wordpress/wp-content `git clone project.git .` pamiętając o wcześniejszym usunięciu wszystkich starych plików i katalogów
2.  Nadać odpowiednie uprawnienia `make wp-chmod`
3.  Wgrać dumpa bazy danych `make wp-install-db dbpath=path/to/db.sql` z podmienionymi wcześniej ścieżkami

## Notatki

- user: "33:33" - www:data
- stdin_open: true - odpowiednik -i w docker exec
- tty: true - odpowiednik -t w docker exec

## Todo:

- [x] poprawić ten opis
- [ ] dodać obsługę certyfikatów Let's Encrypt (https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71)
- [x] poprzerzucać wszystkie hasła, configi i zmienne do ENV
- [ ] obsługa ENV w configu nginx
- [x] zrobic tak, żeby docker-compose czekał na zainstalowanie wordpressa i ładnie o tym informował, a nie sypał błędami, że nie może się połączyć do bazy
- [ ] bind mounts (montowanie do kontenera plików lokalnych a nie w dwie strony)
- [ ] praca na wielu instancjach tego stacka na raz (nginx proxy)
- [ ] dodać ustawianie chmodów przy instalacji z bazy
- [ ] informacja po utworzeniu bazy (teraz trzeba czekać)
- [ ] wgrywanie dumpa bazy z search and replace domeny