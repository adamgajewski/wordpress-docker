#!make
include .env
export $(shell sed 's/=.*//' .env)

docker-start:
	docker-compose up -d
	@echo "\033[92mIf it is a fresh beginning wait a while until database has been prepared.\033[0m"

docker-stop:
	docker-compose stop

clean:
	docker-compose rm -f
	rm -rf ./wordpress ./mysql ./logs
	docker-compose down --volumes

wp-install:
	docker-compose run --rm wp-cli wp core install --prompt

wp-install-prod:
	docker-compose run --rm wp-cli \
	wp core install \
		--path="/var/www/html" \
		--url=$$APP_URL \
		--title="Local Wordpress By Docker" \
		--admin_user=$$WP_ADMIN_USER \
		--admin_password=$$WP_ADMIN_PASS \
		--admin_email=$$WP_ADMIN_EMAIL \
		--skip-email
	docker-compose run --rm wp-cli \
		wp theme install blankslate --activate
	docker-compose run --rm wp-cli \
		wp plugin delete akismet
	docker-compose run --rm wp-cli \
		wp theme delete twentysixteen
	docker-compose run --rm wp-cli \
		wp theme delete twentyseventeen
# 	docker-compose run --rm wp-cli \
# 		wp plugin install woocommerce --activate

wp-install-db:
	docker-compose run --rm wp-cli wp db drop --dbuser=$$DB_USER --dbpass=$$DB_ROOT_PASSWORD
	docker-compose run --rm wp-cli wp db create --dbuser=$$DB_USER --dbpass=$$DB_ROOT_PASSWORD
	docker-compose run --rm wp-cli wp db import $(dbpath) --dbuser=$$DB_USER --dbpass=$$DB_ROOT_PASSWORD

wp-chmod:
	docker-compose run --rm wp-cli mkdir /var/www/html/wp-content/plugins
	docker-compose run --rm wp-cli chown -R www-data:www-data /var/www/html
	docker-compose run --rm wp-cli find /var/www/html -type d -exec chmod 0755 {} \;
	docker-compose run --rm wp-cli find /var/www/html -type f -exec chmod 644 {} \;
	docker-compose run --rm wp-cli wp config set FS_METHOD direct --raw

wp-cli-sh:
	docker-compose run --rm wp-cli /bin/sh